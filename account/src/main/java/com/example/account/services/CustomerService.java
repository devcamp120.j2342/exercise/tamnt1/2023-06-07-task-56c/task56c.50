package com.example.account.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.account.Model.Customer;

@Service
public class CustomerService {
    public List<Customer> createCustomer() {

        ArrayList<Customer> customerList = new ArrayList<>();
        Customer customer1 = new Customer(1, "tam", 20);
        Customer customer2 = new Customer(1, "2", 30);
        Customer customer3 = new Customer(1, "tam", 40);
        customerList.addAll(Arrays.asList(customer1, customer2, customer3));

        return customerList;
    }
}
